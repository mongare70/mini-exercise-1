package com.example.miniexercise1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showSecondActivity(View v){
        startActivity(new Intent(MainActivity.this, SecondActivity.class));
    }

    public void showThirdActivity(View v){
        startActivity(new Intent(MainActivity.this, ThirdActivity.class));
    }

    public void showFourthActivity(View v){
        startActivity(new Intent(MainActivity.this, FourthActivity.class));
    }
}
