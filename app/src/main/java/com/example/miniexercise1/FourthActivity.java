package com.example.miniexercise1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

public class FourthActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fourth_layout);
    }

    public void showMainActivity(View v){
        startActivity(new Intent(FourthActivity.this, MainActivity.class));
    }
}
